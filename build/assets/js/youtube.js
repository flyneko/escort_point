function VideoPlayer(el, options) {
    function Q() {
        this.el = el;
        this.$el = $(this.el);

        this._buildContainer();
        this._buildEvents();
    }

    Q.prototype._buildContainer = function () {
        this.$el.addClass('video');
        this.$container = $('<div class="video__container">').appendTo(this.$el);
        this.$overlay = $('<div class="video__overlay">').appendTo(this.$el);
        this.$button = $('<div class="video__btn">').appendTo(this.$overlay);
        this.$duration = $('<span class="video__duration">').appendTo(this.$overlay);
        this.$duration.html('<div class="loader"><span></span><span></span><span></span><span></span></div>');
    }

    Q.prototype._buildEvents = function () {

    }

    Q.prototype.onLoad = function () {
        var self = this;
        this.$duration.html(fancyTimeFormat(this.player.duration()));
        this.$button.show();
        this.$overlay.click(function () {
            if (self.$el.hasClass('is-playing'))
                self.player.pause();
            else
                self.player.play();
        });
    };

    Q.prototype.onPlay = function () {
        this.$el.addClass('is-playing').removeClass('is-pause');
    }

    Q.prototype.onPause = function () {
        this.$el.removeClass('is-playing').addClass('is-pause');
    }

    Q.prototype.onEnd = function () {
        this.$el.removeClass('is-playing').removeClass('is-pause');
    }

    return new Q();
}

function YouTubePlayer(el, options) {
    function getYoutubeId(url){
        var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
        var match = url.match(regExp);
        return (match&&match[7].length==11)? match[7] : false;
    }

    function injectYoutubeAPI() {
        if ($('#youtube_api').length)
            return;
        var $tag = $('<script>', { id: 'youtube_api', src: 'https://www.youtube.com/player_api' });
        $tag.appendTo('head');
    }

    function Q() {
        var self = this;
        this.el = el;
        this.$el = $(this.el);
        this.url = options && options.url ? options.url : this.$el.data('yt');
        this.ytId = getYoutubeId(this.url);
        this.state = 'valid'
        if (!this.ytId) {
            this.state = 'invalid';
            return false;
        }

        injectYoutubeAPI();
        if (!window.YOTUBE_API_READY)
            window.onYouTubePlayerAPIReady = function () {
                window.YOTUBE_API_READY = true;
                self._buildContainer();
                self._buildEvents();
            };
        else {
            this._buildContainer();
            this._buildEvents();
        }
    }

    Q.prototype._buildContainer = function () {
        var self = this;
        this.$el.addClass('video');
        this.$container = $('<div class="video__container">').appendTo(this.$el);
        this.$overlay = $('<div class="video__overlay">').appendTo(this.$el);
        this.$button = $('<div class="video__btn">').appendTo(this.$overlay);
        this.$duration = $('<span class="video__duration">').appendTo(this.$overlay);
        this.$cover = $('<img class="video__cover" src="' + 'https://img.youtube.com/vi/' + this.ytId + '/maxresdefault.jpg' + '">').appendTo(this.$overlay)
        this.$duration.html('<div class="loader"><span></span><span></span><span></span><span></span></div>');
    }

    Q.prototype._buildEvents = function () {
        var self = this;

        this.$cover.on('load', function (e) {
            var calculatePlayerHeight = function () {
                self.$el.css('height', self.$el.width() * (e.target.naturalHeight / e.target.naturalWidth));
            };

            calculatePlayerHeight();
            $(window).resize(calculatePlayerHeight);
        });

        this.player = new YT.Player(this.$container[0], {
            height: '100%',
            width: '100%',
            host: 'https://www.youtube.com',
            videoId: this.ytId,  // youtube video id
            playerVars: {
                autoplay: 0,
                rel: 0,
                showinfo: 0,
                controls: true
            },
            events: {
                'onReady': onPlayerReady,
                'onStateChange': onPlayerStateChange
            }
        });

        function onPlayerReady(e) { self.onLoad(); }
        function onPlayerStateChange(e) { self.onStateChange(e); }
    }

    Q.prototype.onLoad = function () {
        var self = this;
        this.$duration.html(fancyTimeFormat(this.player.getDuration()));
        this.$button.show();
        this.$overlay.click(function () {
            if (self.$el.hasClass('is-playing')) {
                self.$el.removeClass('is-playing').addClass('is-pause');
                self.player.pauseVideo();
            } else {
                self.$el.addClass('is-playing').removeClass('is-pause');
                self.player.playVideo();
            }
        })
    };

    Q.prototype.onStateChange = function (e) {
        if (e.data == YT.PlayerState.ENDED) {
            this.$el.removeClass('is-playing').removeClass('is-pause');
        }
    }

    return new Q();
}