$(function () {

    function initAttachments($el) {
        var $parent = $el || $('body');
        $parent.find('.js-video:visible').each(function () {
            var $this = $(this);
            var src = $this.data('src');
            var player = VideoPlayer;
            if ($this.data('video-init'))
                return;
            if (src.match('youtube'))
                player = YoutubePlayer;
            new player(this, { src: src });
            $this.data('video-init', true);
        });

        $parent.find('.js-audio:visible').each(function () {
            var $this = $(this);
            if ($this.data('audio-init'))
                return;
            new AudioPlayer(this);
            $this.data('audio-init', true);
        });

        $parent.find('.js-img:visible').each(function () {
            var $this = $(this);
            if ($this.data('audio-init'))
                return;
            $this.click(function (e) {
                BigPicture({ el: this, imgSrc: $this.attr('href') });
                e.preventDefault();
                return false;
            });
            $this.data('img-init', true);
        });
    }

    $('.js-dropdown').dropdown();

    $(".js-modal").fancybox({
        touch: false
    });

    $('.js-select').each(function () {
        var dropkick = new Dropkick(this, {
            mobile: false
        });
        $(this).data('dk', dropkick);
    });

    $('[data-full-height]').each(function () {
        var $this = $(this);
        $this.css('min-height', window.innerHeight - $('.footer').outerHeight() - $('.header').outerHeight() - $('.group__title').outerHeight() - 45);
    });

    $('.js-datepicker').each(function() {
        var $this = $(this);
        var time = !!$this.data('time');
        var options = {
            timepicker: time,
            language: 'en'
        }
        $this.datepicker(options);
    })

    $('.js-switcher').each(function () {
        var $checkbox = $(this);
        var $parent = $checkbox.closest($checkbox.data('switch-parent'));
        var $target = $parent.find('[data-switch-target]');

        $checkbox.change(function () {
            if (this.checked)
                $target.removeClass('d-none');
            else
                $target.addClass('d-none');
        })
    });

    (function () {
        function setCaretPosition(ctrl, pos) {
            // Modern browsers
            if (ctrl.setSelectionRange) {
                ctrl.focus();
                ctrl.setSelectionRange(pos, pos);

                // IE8 and below
            } else if (ctrl.createTextRange) {
                var range = ctrl.createTextRange();
                range.collapse(true);
                range.moveEnd('character', pos);
                range.moveStart('character', pos);
                range.select();
            }
        }

        function getCaretPosition (input) {
            if (!input) return;
            if ('selectionStart' in input) {
                return input.selectionStart;
            } else if (document.selection) {
                // IE
                input.focus();
                var sel = document.selection.createRange();
                var selLen = document.selection.createRange().text.length;
                sel.moveStart('character', -input.value.length);
                return sel.text.length - selLen;
            }
        }

        $('.js-mask').each(function () {
            var $this = $(this);
            var mask = $this.data('mask');

            if (mask == 'phone') {
                //var placeholder = "+7 (___) ___-__-__";
                //$this.mask("+7 (999) 999-99-99", { placeholder: placeholder });
            } else {
                $this.focus(function () {
                    if (this.value != '')
                        return;
                    $this.val(mask);
                }).focusout(function () {
                    if (this.value == mask)
                        this.value = '';
                }).on('input', function () {
                    if (this.value.length < mask.length)
                        this.value = mask;
                }).on('mousedown click mouseup', function () {
                    setTimeout(function () {
                        if (getCaretPosition($this[0]) < mask.length)
                            setCaretPosition($this[0], mask.length);
                    });
                });
            }

        })
    })();

    $('.js-replace').widthReplace();

    (function () {
        var $select = $('.js-city-select');
        $select.focusin(function () {
            $select.parent().addClass('is-active');
        }).focusout(function () {
            $select.parent().removeClass('is-active');
        }).change(function () {
            $select.parent().find('> span').html(this.value);
        });
    })();


    (function () {
        var $carousel = $('.js-model-photo');
        $carousel.each(function () {
            var $this = $(this);
            var $model = $this.closest('.model');
            SwiperProxy($this, {
                dots: true,
                nav: false,
                slidesPerView: 1,
                pagination: {
                    clickable: true
                },
                speed: 600,
                loop: true,
                spaceBetween: 0,
                on: {
                    init: function () {
                        function pos() {
                            $model.find('.swiper-dots').css('bottom', $model.find('.model__info').outerHeight());
                        }
                        pos();
                        $(window).resize(pos);
                    },
                },
            });
        });
    })();

    (function () {
        autosize($('.js-autoheight'));
    })();

    (function () {
        initAttachments();
        $('.entry__more').click(function () {
            var $entry = $(this).closest('.entry');
            $entry.find('.entry__hidden > *').unwrap();
            $(this).remove();
            initAttachments($entry);
            return false;
        })
    })();

    (function () {
        var $actions = $('.js-reply-comment');

        $('.entry__comments').on('click', '.comment__options', function () {
            var $comment = $(this).closest('.comment');
            $actions.insertAfter($comment.find('> .comment__body')).show();
            return false;
        });

        $('.comment-actions__close').click(function () {
            $actions.hide();
            return false;
        });

        $('.entry__comments').on('click', '.comment__more', function () {
            var $this = $(this);
            var $comment = $this.closest('.comment');
            var $children = $comment.find('> .comment__children');
            $children.removeClass('d-none');
            initAttachments($children);
            $this.hide();
            return false;
        });

        $('.entry__comments').on('click', '.comment__close-branch', function () {
            var $this = $(this);
            var $comment = $this.closest('.comment');
            var $children = $comment.find('> .comment__children');
            $children.addClass('d-none');
            $comment.find('> .comment__more').show();
            return false;
        });
    })();

    (function () {
        $('.js-tags').each(function () {
            var $container = $(this);
            var $input = $container.find('[data-tags=input]');
            var $button = $container.find('[data-tags=trigger]');
            var $area = $container.find('[data-tags=area]');

            $button.click(function () {
                if ($input.val().trim() == '')
                    return;

                var $item = $('<div class="tags__item">');
                var $itemRemove = $('<button type="button" class="tags__item-remove">').click(function () {
                    $item.remove();
                    if (!$area.find('> *').length)
                        $area.addClass('d-none');
                }).appendTo($item);
                var $tag = $('<a href="#" class="tag">').html($input.val()).appendTo($item);
                $input.val('');
                $area.removeClass('d-none').append($item);
            });
        })
    })();

    // Form handlers
    (function () {
        var $area = $('.form__attachments-area');

        $('.form').submit(function () {
           var valid = true;
           var $form = $(this);

            $form.find('[data-required]').each(function () {
                var $this = $(this);
                var $parent = $this.data('required-parent') ? $this.closest($this.data('required-parent')) : $this;
                var $error = ($this.data('required-parent') ? $parent : $this).parent().find('.form__error');
                if (this.value.trim() == '') {
                   if (!$error.length)
                       $error = $('<span class="form__error">').html('This field is required').insertAfter($parent);
                   $error.show();
                   valid = false;
                } else
                    $error.hide();
            });
            //return valid;
            return false;
        });

        $('.js-attachment-add').click(function () {
            var $textTrigger = $('.js-attachment-add[data-type=text]');
            var type = $(this).data('type');
            var createItem = function () {
                var $item = $('<div class="form__attachments-item">');
                var $itemRemove = $('<button class="form__attachments-remove">').click(function () {
                    $item.remove();
                    if (!$area.find('> *').length)
                        $area.hide();
                }).appendTo($item);
                return $item;
            }


            switch (type) {
                case 'text':
                    var $textarea = $('<textarea class="input" rows="3">');
                    var $item = createItem().append($textarea);
                    autosize($textarea);
                    $area.show().append($item);
                    $textTrigger.attr('disabled', true).prop('disabled', true);
                    break;
                case 'video':
                    var $input = $('<input class="input" type="text" placeholder="Link on video http://...">');
                    var $item = createItem().append($input);
                    $input.change(function (e) {
                        var $video = $('<div>').appendTo($item);
                        setTimeout(function () {
                            var player = new YoutubePlayer($video[0], { src: e.target.value });
                            if (player.state == 'valid') {
                                //$video.addClass('m-t-10');
                                $input.remove();
                            } else
                                $video.remove();
                        }, 0);
                    });
                    $area.css('display', 'flex').append($item);
                    $textTrigger.attr('disabled', false).prop('disabled', false);
                    break;
                case 'audio':
                case 'image':
                    var $input = $('<input type="file" multiple accept="' + type + '/*">').hide().appendTo($area).click();
                    $input.change(function () {
                        if (!this.files.length)
                            return;
                        var $this = $(this);
                        for (var i = 0; i < this.files.length; i++) {
                            var reader = new FileReader();
                            reader.addEventListener("load", function (event) {
                                var file = event.target;
                                var $item = createItem();
                                switch (type) {
                                    case 'image':
                                        var $img = $('<img>', { src: file.result });
                                        $item.addClass('is-img');
                                        $img.click(function () {
                                            BigPicture({
                                                el: this,
                                                imgSrc: file.result
                                            });
                                        }).appendTo($item);
                                        break;
                                    case 'audio':
                                        var $audio = $('<div>').appendTo($item);
                                        setTimeout(function () {
                                            new AudioPlayer($audio, { src: file.result, force: true });
                                        }, 0);
                                        break;
                                }
                                $area.css('display', 'flex').append($item);
                                $textTrigger.attr('disabled', false).prop('disabled', false);
                                $this.remove();
                            });
                            reader.readAsDataURL(this.files[i]);
                        }
                    });
                    break;
            }
        });
    })();

    // Editor
    (function() {
        $('.js-editor').each(function () {
            var $editor = $(this);
            var $toolbar = $('<div class="ql-toolbar">').insertAfter($editor).html(
                '<div class="ql-group">' +
                '<button class="ql-bold ql-btn"><i class="fas fa-bold"></i></button>' +
                '<button class="ql-italic ql-btn"><i class="fas fa-italic"></i></button>' +
                '<button class="ql-underline ql-btn"><i class="fas fa-underline"></i></button>' +
                '<button class="ql-strike ql-btn"><i class="fas fa-strikethrough"></i></button>' +
                '</div>' +
                //'<div class="ql-group">' +
                //'<button class="ql-bold ql-btn"><i class="fas fa-undo"></i></button>' +
                //'<button class="ql-italic ql-btn"><i class="fas fa-redo"></i></button>' +
                //'</div>' +
                '<div class="ql-group">' +
                '<button class="ql-list ql-btn" value="bullet"><i class="fas fa-list-ul"></i></button>' +
                '<button class="ql-list ql-btn" value="ordered"><i class="fas fa-list-ol"></i></button>' +
                '</div>'
            );
            var quill = new Quill(this, {
                modules: { toolbar: $toolbar[0]}
            });
        })
    })();

    // Dynamic form
    (function () {
        $('.js-dynamic').dynamic();


        $('.js-language-level').on('beforeProcessTemplate', function (e, content) {
            content.template = content.template.replace('{level}', '<div class="levels active-' + content.fields.level + '">' +
                '<div class="levels__item"><div class="levels__dot"></div></div>' +
                '<div class="levels__item"><div class="levels__dot"></div></div>' +
                '<div class="levels__item"><div class="levels__dot"></div></div>' +
                '<div class="levels__item"><div class="levels__dot"></div></div>' +
                '</div>');
        });

        $('.js-services').on('beforeAdd', function (e, $template) {
            $template.find('.js-service-plus').click(function () {
                $(this).parent().find('> input').removeClass('d-none');
                $(this).remove();
            });
        });
    })();

    (function () {
        $('.js-media-upload').each(function () {
            var $this = $(this),
                $block = $this.closest('.upload-block'),
                $results = $block.find('[data-media=area]'),
                startAreaHtml = $results.html(),
                $reset = $('<a href="#" class="upload-block__remove">').appendTo($results),
                template = $block.find('[data-media=template]').html(),
                maxCount = $this.data('maxcount') || 1,
                maxSize = $this.data('maxsize'),
                type = $this.data('type'),
                minDim = $this.data('mindim') ? $this.data('mindim').toString().split('x') : [],
                maxDim = $this.data('maxdim') ? $this.data('maxdim').toString().split('x') : [];

            var validUpload = function ($t) {
                $results.show();
                $reset.show().click(function () {
                    $results.hide();
                    $reset.hide().insertBefore($results);
                    $results.html(startAreaHtml).append($reset);
                    $this[0].value = null;
                    return false;
                });
                $this.trigger('uploaded', [$t]);
            }
            var ierror = function (msg) {
                var $error = $block.find('.upload-block__error');
                if (!$error.length)
                    $error = $('<div class="upload-block__error">').insertBefore($results);
                if (msg === false) {
                    $error.hide();
                    return false;
                }
                $error.html(msg).show();
                return false;
            };

            $this.change(function () {
                if (this.files && this.files.length) {
                    var $appendTo = $this.data('append') ? $($this.data('append')) : null;
                    var totalSize = 0;
                    ierror(false);

                    if (this.files.length > maxCount)
                        return ierror('You can not select more than ' + maxCount + ' files');

                    for (var i = 0; i < this.files.length; i++)
                        totalSize += ((this.files[i].size / 1024) / 1024).toFixed(4); // MB

                    if (totalSize > maxSize)
                        return ierror('File size limit exceeded');

                    for (var i = 0; i < this.files.length; i++) {
                        if (!type || type == 'photo') {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                var image = new Image();
                                image.src = e.target.result;
                                image.onload = function () {
                                    if (minDim.length && (minDim[0] > this.width || (minDim[1] && minDim[1] > this.height)))
                                        return ierror('Размер одного из файлов не соответствует минимальному');
                                    if (maxDim.length && (maxDim[0] < this.width || maxDim[1] < this.height))
                                        return ierror('Размер одного из изображений превышает ' + maxDim.join('x'));
                                    if ($appendTo && $appendTo.length) {
                                        $appendTo.attr('src', e.target.result);
                                        validUpload($appendTo);
                                    } else {
                                        var $row = $results.find('> .row');
                                        if (!$results.find('> .row').length)
                                            $row = $('<div class="row">').prependTo($results);
                                        template = template.replace('{img}', e.target.result);
                                        var $template = $(template);
                                        $template.appendTo($row);
                                        validUpload($template);
                                    }
                                };
                            }
                            reader.readAsDataURL(this.files[i]);
                        } else if (type == 'video') {
                            if ($appendTo && $appendTo.length) {
                                new VideoPlayer($appendTo[0], { src: this.files[i] });
                                validUpload($appendTo);
                            }
                        }
                    }
                }
            });
        });

        $('.js-youtube-profile').on('change', function () {
            var $container = $('#youtube-profile');
            if (this.value.trim() == '') {
                $container.addClass('d-none');
                return;
            }

            var player = new YoutubePlayer($container[0], { src: this.value });
            if (player.state == 'valid')
                $container.empty().removeClass('d-none');
        });
    })();

    (function () {
        $('.profile-photo').click(function () {
            var $parent = $(this).closest('.row');
            $parent.find('.is-main:not(.example)').removeClass('is-main');
            $(this).addClass('is-main');
        });
    })();
});