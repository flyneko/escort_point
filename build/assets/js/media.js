(function () {
    function MediaAbstract(el, options) {
        this.options        = options || {};
        this.el             = el;
        this.type           = null;
        this.$el            = $(el);
        this.src            = options.src || this.$el.data('src');
        this.playInterval   = null;
        this.hintTimeout    = null;
        this.nPlayer        = null;
        this.controls       = typeof options.controls == 'boolean' ? options.controls : true;
        this.player         = {
            play: function () {},
            pause: function () {},
            setSeek: function () {},
            getSeek: function () {},
            getVolume: function () {},
            getDuration: function () {},
            setVolume: function () {}
        };
    };

    MediaAbstract.prototype._fancyTimeFormat = function (time) {
        var hrs = ~~(time / 3600);
        var mins = ~~((time % 3600) / 60);
        var secs = Math.ceil(time % 60);

        var ret = "";
        if (hrs > 0)
            ret += "" + hrs + ":";

        ret += (mins < 10 ? "0" : "") + mins + ":" + (secs < 10 ? "0" : "");
        ret += secs;
        return ret;
    }

    MediaAbstract.prototype._setTrackPosition = function ($container, pos)  {
        $container.find('.media__line-current').css('width', pos + '%');
    }

    MediaAbstract.prototype._buildContainer = function () {
        this.$el.addClass(this.type).addClass('media');
        if (this.controls) {
            this.$controls = $('<div class="media__controls">').appendTo(this.$el);
            this.$loader = this.options.loader ? $('<div class="media__loader">').html('<div class="loader"><span></span><span></span><span></span><span></span></div>').appendTo(this.$controls) : null;
            this.$button = $('<button type="button" class="media__btn">').prependTo(this.$controls);
            this.$progress = $('<div class="media__progress">').html('<div class="media__line"></div>').appendTo(this.$controls);
            this.$progressLine = $('<div class="media__line-current">').appendTo(this.$progress.find('.media__line'));
            this.$progressDragger = $('<div class="media__slider">').appendTo(this.$progress.find('.media__line'));
            this.$volume = $('<div class="media__volume">').html('<div class="media__line"></div>').appendTo(this.$controls);
            this.$volumeLine = $('<div class="media__line-current">').appendTo(this.$volume.find('.media__line'));
            this.$volumeDragger = $('<div class="media__slider">').appendTo(this.$volume.find('.media__line'));
            this.$duration = $('<div class="media__duration">').appendTo(this.$progress);
            this.$hint = $('<div class="media__hint">').appendTo(this.$controls);
            this.progressSlider = RS(this.$progress.find('.media__line'));
            this.volumeSlider = RS(this.$volume.find('.media__line'));
        }
    };

    MediaAbstract.prototype._buildEvents = function () {
        if (this.controls) {
            this.$button.click(this.toggle.bind(this));
            this.volumeSlider.on('drag', this.onVolumeChange.bind(this));
            this.progressSlider.on('drag', this.onProgressChange.bind(this));
        }
    };

    MediaAbstract.prototype.toggle = function () {
        this.player[this.isPlaying ? 'pause' : 'play']();
    }

    MediaAbstract.prototype.showTooltip = function (content, $attach) {
        var self = this;
        clearTimeout(this.hintTimeout);
        this.$hint.html(content).show().insertBefore($attach);
        this.$hint.css({
            top: $attach.position().top - this.$hint.outerHeight() - ($attach.height() / 2),
            left: $attach.position().left - (this.$hint.width() / 2)
        })
        this.hintTimeout = setTimeout(function () {
            self.$hint.hide();
        }, 200);
    }

    MediaAbstract.prototype.onLoad = function () {
        this.$el.addClass('is-loaded');
        if (this.controls) {
            if (this.options.loader)
                this.$loader.hide();
            this.$duration.html(this._fancyTimeFormat(this.player.getDuration()));
            this.volumeSlider.setPosition(this.player.getVolume());
            this._setTrackPosition(this.$volume, this.player.getVolume());
        }
    }

    MediaAbstract.prototype.onProgressChange = function (value) {
        var timeFormatted = this._fancyTimeFormat(this.player.getSeek());
        this._setTrackPosition(this.$progress, value);
        this.player.setSeek(this.player.getDuration() * (value / 100));
        this.$duration.html(timeFormatted + ' / ' + this._fancyTimeFormat(this.player.getDuration()));
        this.showTooltip(timeFormatted, this.$progressDragger);
    }

    MediaAbstract.prototype.onVolumeChange = function (value) {
        this._setTrackPosition(this.$volume, value);
        this.player.setVolume(value);
        this.showTooltip(value + '%', this.$volumeDragger);
    }

    MediaAbstract.prototype.onPlay = function () {
        var self = this;
        this.$el.addClass('is-playing');
        this.$el.removeClass('is-paused');
        this.isPlaying = true;
        if (this.controls) {
            this.playInterval = setInterval(function () {
                var progress = (self.player.getSeek() / self.player.getDuration()) * 100;
                self.$duration.html(self._fancyTimeFormat(self.player.getSeek()) + ' / ' + self._fancyTimeFormat(self.player.getDuration()));
                self._setTrackPosition(self.$progress, progress);
                self.progressSlider.setPosition(progress);
            }, 100);
        }
    }

    MediaAbstract.prototype.onPause = function () {
        this.$el.removeClass('is-playing');
        this.$el.addClass('is-paused');
        clearInterval(this.playInterval);
        this.isPlaying = false;
    };

    MediaAbstract.prototype.onEnd = function () {
        this.onPause();
        this.$el.removeClass('is-paused');
        if (this.controls) {
            this.$duration.html(this._fancyTimeFormat(this.player.getDuration()));
            this._setTrackPosition(this.$progress, 0);
        }
    };

    function AudioPlayer(el, options) {
        var self = this;
        MediaAbstract.call(this, el, $.extend(options, {
            loader: true
        }));

        this.class_ = Object.getPrototypeOf(this);
        this.SuperClass = Object.getPrototypeOf(this.class_);

        this.type = 'audio';
        this.nPlayer = new Howl({
            src:        this.src,
            autoplay:   false,
            preload:    this.options.force || false,
            loop:       false,
            volume:     0.5
        });

        this.player = {
            play: function () { return self.nPlayer.play() },
            pause: function () { return self.nPlayer.pause() },
            setSeek: function (v) { return self.nPlayer.seek(v) },
            getSeek: function () { return self.nPlayer.seek() },
            getVolume: function () { return self.nPlayer.volume() * 100; },
            getDuration: function () { return self.nPlayer.duration(); },
            setVolume: function (v) { self.nPlayer.volume(v / 100); }
        };

        this._buildContainer();
        this._buildEvents();

        if (this.nPlayer.state() == 'loaded')
            this.onLoad();
    }

    AudioPlayer.prototype = Object.create(MediaAbstract.prototype);
    AudioPlayer.prototype.constructor = AudioPlayer;
    AudioPlayer.prototype._buildEvents = function () {
        this.SuperClass._buildEvents.call(this);
        this.nPlayer.on('play', this.onPlay.bind(this));
        this.nPlayer.on('pause', this.onPause.bind(this));
        this.nPlayer.on('end', this.onEnd.bind(this));
        this.nPlayer.on('load', this.onLoad.bind(this));

        var checkVisibleState = function () {
            var visibleArea = $(document).scrollTop() + window.innerHeight
            if (this.$el.offset().top < visibleArea && this.$el.offset().top + this.$el.height() > $(document).scrollTop() && this.nPlayer.state() == 'unloaded')
                this.nPlayer.load();
        }

        if (!this.options.force) {
            checkVisibleState.call(this);
            $(window).scroll(checkVisibleState.bind(this));
        }
    }

    function AbstractVideo(el, options) {
        MediaAbstract.call(this, el, $.extend(options, {
            loader: false
        }));
        this.type = 'video';
        this.class_ = Object.getPrototypeOf(this);
        this.SuperClass = Object.getPrototypeOf(this.class_);
    }
    AbstractVideo.prototype = Object.create(MediaAbstract.prototype);
    AbstractVideo.prototype.constructor = AbstractVideo;
    AbstractVideo.prototype._buildContainer = function () {
        this.SuperClass._buildContainer.call(this);
        this.$container = $('<div class="video__container">').prependTo(this.$el);
        this.$overlay = $('<div class="video__overlay">').prependTo(this.$el);
        this.$overlayButton = $('<div class="video__btn">').appendTo(this.$overlay);
        this.$overlayDuration = $('<span class="video__duration">').appendTo(this.$overlay);
        this.$cover = $('<img class="video__cover">').appendTo(this.$overlay);
        this.$overlayDuration.html('<div class="loader"><span></span><span></span><span></span><span></span></div>');
        this.$source = $('<div class="video__source">').hide().html(this.source).appendTo(this.$overlay);
        if (this.controls)
            this.$controls.appendTo(this.$container);
    };
    AbstractVideo.prototype._buildEvents = function () {
        this.SuperClass._buildEvents.call(this);
        this.$overlay.click(this.toggle.bind(this));
    };
    AbstractVideo.prototype.onLoad = function () {
        this.SuperClass.onLoad.call(this);
        this.$overlayDuration.html(this._fancyTimeFormat(this.player.getDuration()));
    };

    function VideoPlayer(el, options) {
        var self = this;
        AbstractVideo.call(this, el, $.extend(options, {

        }));

        this.c_ = Object.getPrototypeOf(this);
        this.class_ = Object.getPrototypeOf(this.c_);
        this.SuperClass = Object.getPrototypeOf(this.class_);

        this.nPlayer = document.createElement('video');
        this.nPlayer.preload = 'metadata';
        this.nPlayer.src = typeof this.src == 'string' ? this.src : URL.createObjectURL(this.src);

        this.player = {
            play: function () { return self.nPlayer.play(); },
            pause: function () { return self.nPlayer.pause(); },
            setSeek: function (v) {  self.nPlayer.currentTime = v; },
            getSeek: function () { return self.nPlayer.currentTime; },
            getVolume: function () { return self.nPlayer.volume * 100; },
            getDuration: function () { return self.nPlayer.duration; },
            setVolume: function (v) { self.nPlayer.volume = v / 100; }
        };

        this._buildContainer();
        this._buildEvents();
    }
    VideoPlayer.prototype = Object.create(AbstractVideo.prototype);
    VideoPlayer.prototype.constructor = VideoPlayer;
    VideoPlayer.prototype._buildContainer = function () {
        AbstractVideo.prototype._buildContainer.apply(this);
        $(this.nPlayer).prependTo(this.$container);
        this.$container.css('position', 'relative');
        this.$cover.remove();
        this.$cover = $('<canvas class="video__cover">').appendTo(this.$overlay);
    };
    VideoPlayer.prototype._buildEvents = function () {
        var self = this;
        AbstractVideo.prototype._buildEvents.apply(this);
        this.nPlayer.addEventListener('click', function() { self.toggle(); });
        this.nPlayer.addEventListener('play', function () { self.onPlay(); });
        this.nPlayer.addEventListener('pause', function () { self.onPause(); });
        this.nPlayer.addEventListener('ended', function () { self.onEnd(); });
        this.nPlayer.addEventListener('loadedmetadata', function () { self.onLoad(); });
    };
    VideoPlayer.prototype.onLoad = function () {
        var self = this;
        AbstractVideo.prototype.onLoad.apply(this);
        this.$cover[0].width = this.$cover.width();
        this.$cover[0].height = this.$cover.height();
        setTimeout(function () {
            self.$cover[0].getContext("2d").drawImage(self.nPlayer, 0, 0, self.$cover.width(), self.$cover.height());
        }, 100);
    };




    function YoutubePlayer(el, options) {
        AbstractVideo.call(this, el, $.extend(options, {
            controls: false
        }));

        this.state = 'valid';
        this.source = 'YouTube';
        this.c_ = Object.getPrototypeOf(this);
        this.class_ = Object.getPrototypeOf(this.c_);
        this.SuperClass = Object.getPrototypeOf(this.class_);

        this.ytId = (function() {
            var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
            var match = this.src.match(regExp);
            return (match && match[7].length==11)? match[7] : false;
        }).call(this);

        if (!this.ytId) {
            this.state = 'invalid';
            return false;
        }

        if (!window.YT) {
            window.onYouTubePlayerAPIReady = this._buildContainer.bind(this);
            $('<script>', { src: '//www.youtube.com/iframe_api'}).appendTo('head');
        } else
            this._buildContainer();
    }
    YoutubePlayer.prototype = Object.create(AbstractVideo.prototype);
    YoutubePlayer.prototype.constructor = YoutubePlayer;
    YoutubePlayer.prototype._buildContainer = function () {
        var self = this;
        AbstractVideo.prototype._buildContainer.apply(this);

        this.$cover.attr('src', 'https://img.youtube.com/vi/' + this.ytId + '/0.jpg');
        this.$source.show();
        var calculatePlayerHeight = function () { self.$el.css('height', self.$el.width() * 0.5625); };
        calculatePlayerHeight();
        $(window).resize(calculatePlayerHeight);

        this.nPlayer = new YT.Player($('<div>').appendTo(this.$container)[0], {
            height: '100%',
            width: '100%',
            videoId: this.ytId,  // youtube video id
            playerVars: {
                autoplay: 0,
                rel: 0,
                showinfo: 0,
                controls: 2
            },
        });
        this.nPlayer.addEventListener('onReady', function () { self._buildEvents(); self.onLoad(); });

        this.player = {
            play: function () { return self.nPlayer.playVideo(); },
            pause: function () { return self.nPlayer.pauseVideo(); },
            setSeek: function (v) { return self.nPlayer.seekTo(v); },
            getSeek: function () { return self.nPlayer.getCurrentTime(); },
            getVolume: function () { return self.nPlayer.getVolume(); },
            getDuration: function () { return self.nPlayer.getDuration(); },
            setVolume: function (v) { self.nPlayer.setVolume(v); }
        };
    };
    YoutubePlayer.prototype._buildEvents = function () {
        var self = this;

        AbstractVideo.prototype._buildEvents.apply(this);
        this.nPlayer.addEventListener('onStateChange', function (e) {
            if (e.data == YT.PlayerState.ENDED)
                self.onEnd();
            if (e.data == YT.PlayerState.PLAYING)
                self.onPlay();
            if (e.data == YT.PlayerState.PAUSED)
                self.onPause();
        });
    };

    function RS($target, event, vertical) {
        event = event || {};
        var target = $target[0];
        var win = window,
            doc = document,
            ranger = $target[0],
            dragger = $target.find('.media__slider')[0],
            drag = false,
            rangerSize = 0,
            draggerSize = 0,
            rangerDistance = 0,
            cacheValue = 0,
            vertical = vertical || event.vertical || false,
            size = vertical ? 'offsetHeight' : 'offsetWidth',
            css = vertical ? 'top' : 'left',
            page = vertical ? 'pageY' : 'pageX',
            offset = vertical ? 'offsetTop' : 'offsetLeft',
            client = vertical ? 'clientY' : 'clientX',
            scroll = vertical ? 'scrollTop' : 'scrollLeft';
            
        function isSet(x) { return typeof x !== "undefined"; }
        function isFunc(x) { return typeof x === "function"; }
        function getCoordinate(el) { var x = el[offset]; while (el = el.offsetParent) x += el[offset]; return x; }
        function on(ev, el, fn) { if (el.addEventListener) el.addEventListener(ev, fn, ev.match('touch') ? { passive: false } : false); else if (el.attachEvent) el.attachEvent('on' + ev, fn); else el['on' + ev] = fn; }
        function off(ev, el, fn) { if (el.removeEventListener) el.removeEventListener(ev, fn); else if (el.detachEvent) el.detachEvent('on' + ev, fn); else el['on' + ev] = null; }
        function edge(a, b, c) { if (a < b) return b; if (a > c) return c; return a; }

        if (isFunc(event)) event = { drag: event };
        
        function setSize() {
            rangerSize = ranger[size];
            rangerDistance = getCoordinate(ranger);
            draggerSize = dragger[size];
        }

        function setPosition(v) {
            setSize();
            cacheValue = edge(isSet(v) ? v : 0, 0, 100);
            dragger.style[css] = (((cacheValue / 100) * rangerSize) - (draggerSize / 2)) + 'px';
        }

        function dragInit() {
            setPosition(event.value);
            if (isFunc(event.create)) event.create(cacheValue, target);
            if (isFunc(event.drag)) event.drag(cacheValue, target);
        }

        function dragStart(e) {
            setSize(), drag = true, dragUpdate(e);
            on("touchmove", doc, dragMove);
            on("mousemove", doc, dragMove);
            if (isFunc(event.start)) event.start(cacheValue, target, e);
            //return e.preventDefault(), false;
        }

        function dragMove(e) {
            dragUpdate(e);
            //return e.preventDefault(), false;
        }

        function dragStop(e) {
            drag = false;
            off("touchmove", doc, dragMove);
            off("mousemove", doc, dragMove);
            if (isFunc(event.stop)) event.stop(cacheValue, target, e);
            //return e.preventDefault(), false;
        }

        function dragUpdate(e) {
            e = e || win.event;
            var pos = e.touches ? e.touches[0][page] : e[page],
                move = edge(pos - rangerDistance, 0, rangerSize),
                value = edge(((pos - rangerDistance) / rangerSize) * 100, 0, 100);
            if (!pos) pos = e[client] + doc.body[scroll] + doc.documentElement[scroll];
            if (drag) {
                dragger.style[css] = (move - (draggerSize / 2)) + 'px';
                cacheValue = Math.round(value);
                if (isFunc(event.drag)) event.drag(cacheValue, target, e);
            }
        }

        on("touchstart", ranger, dragStart);
        on("mousedown", ranger, dragStart);

        on("touchend", doc, dragStop);
        on("mouseup", doc, dragStop);

        on("resize", win, function(e) {
            setSize(), drag = false;
            dragger.style[css] = (((cacheValue / 100) * rangerSize) - (draggerSize / 2)) + 'px';
        });
        return setSize(), dragInit(),{
            setPosition: setPosition,
            on: function (ev, func) {
                event[ev] = func;
            }
        };
    }

    return window.AudioPlayer = AudioPlayer, window.YoutubePlayer = YoutubePlayer, window.VideoPlayer = VideoPlayer;
})();