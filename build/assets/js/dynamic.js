(function($) {
    $.fn.dynamic = function() {
        var $all = $(this);
        this.each(function () {
            var $this = $(this);
            var $trigger = $this.find('[data-dynamic=trigger]');
            var $fields = $this.find('[data-dynamic-field]');
            var template = $this.find('[data-dynamic=template]').html();
            var $area = $this.find('[data-dynamic=area]').hide();
            var $areaRow = $('<div class="row">').appendTo($('<div class="form__dynamics">').appendTo($area));
            var activeHandler = true;
            var content = {
                template: template,
                fields: {}
            };
            var disabled = function (state) {
                if ($fields.length == 1)
                    return;
                var $label = $trigger.parent().find('> label');
                $label[state ? 'addClass' : 'removeClass']('is-disabled');
                if ($trigger.is('select')) {
                    if (state)
                        $trigger.data('dk').disable();
                    else
                        $trigger.data('dk').disable(false);
                } else
                    $trigger.attr('disabled', state).prop('disabled', state);
            }

            disabled(true);

            $fields.each(function () {
                var $f = $(this);
                $f.on($f.is('select') ? 'change' : 'input', function () {
                    var valid = true;
                    $fields.each(function () {
                        if (this.value.trim() == '' && !$(this).is($trigger))
                            valid = false;
                        content.fields[$(this).data('dynamic-field')] = this.value;
                    });
                    if (!valid)
                        disabled(true);
                    else
                        disabled(false);
                });
            })

            template = template.replace('{remove}', '<button type="button" class="form__dynamics-remove"><i class="far fa-trash-alt"></i></button>');


            $trigger.on($trigger.is('button') ? 'click' : 'change', function () {
                if (!activeHandler)
                    return;
                content.template = template;

                $this.trigger("beforeProcessTemplate", [content] );

                $fields.each(function () {
                    content.template = content.template.replace('{' + $(this).data('dynamic-field') + '}', this.value);
                });
                var $template = $(content.template).addClass('form__dynamics-item');
                $this.trigger("beforeAdd", [$template] );
                $template.appendTo($areaRow);
                $area.show();
                $template.find('.form__dynamics-remove').click(function () {
                    $template.remove();
                    if (!$areaRow.find('> *').length)
                        $area.hide();
                    else
                        $area.show();
                });

                $fields.each(function () {
                    activeHandler = false;
                    var $this = $(this);
                    if ($this.is('select'))
                        $this.data('dk').select('');
                    else
                        $this.val('');
                    setTimeout(function () {
                        activeHandler = true;
                    }, 0);
                })
                disabled(true);
            });
        });
    }
})(jQuery);