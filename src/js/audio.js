function AudioPlayer(el, options) {
    function RS($target, event, vertical) {

        event = event || {};
        var target = $target[0];
        var win = window,
            doc = document,
            ranger = $target[0],
            dragger = $target.find('.audio__slider')[0],
            drag = false,
            rangerSize = 0,
            draggerSize = 0,
            rangerDistance = 0,
            cacheValue = 0,
            vertical = vertical || event.vertical || false,
            size = vertical ? 'offsetHeight' : 'offsetWidth',
            css = vertical ? 'top' : 'left',
            page = vertical ? 'pageY' : 'pageX',
            offset = vertical ? 'offsetTop' : 'offsetLeft',
            client = vertical ? 'clientY' : 'clientX',
            scroll = vertical ? 'scrollTop' : 'scrollLeft';

        function isSet(x) {
            return typeof x !== "undefined";
        }

        function isFunc(x) {
            return typeof x === "function";
        }

        function getCoordinate(el) {
            var x = el[offset];
            while (el = el.offsetParent)
                x += el[offset];
            return x;
        }

        function on(ev, el, fn) {
            if (el.addEventListener)
                el.addEventListener(ev, fn, ev.match('touch') ? { passive: false } : false);
            else if (el.attachEvent)
                el.attachEvent('on' + ev, fn);
            else
                el['on' + ev] = fn;
        }

        function off(ev, el, fn) {
            if (el.removeEventListener)
                el.removeEventListener(ev, fn);
            else if (el.detachEvent)
                el.detachEvent('on' + ev, fn);
            else
                el['on' + ev] = null;
        }

        // `RS(target, function(a, b, c) {})`
        if (isFunc(event)) {
            event = {
                drag: event
            };
        }

        function edge(a, b, c) {
            if (a < b) return b;
            if (a > c) return c;
            return a;
        }

        function setSize() {
            rangerSize = ranger[size];
            rangerDistance = getCoordinate(ranger);
            draggerSize = dragger[size];
        }

        function setPosition(v) {
            cacheValue = edge(isSet(v) ? v : 0, 0, 100);
            dragger.style[css] = (((cacheValue / 100) * rangerSize) - (draggerSize / 2)) + 'px';
        }

        function dragInit() {
            setPosition(event.value);
            if (isFunc(event.create)) event.create(cacheValue, target);
            if (isFunc(event.drag)) event.drag(cacheValue, target);
        }

        function dragStart(e) {
            setSize(), drag = true, dragUpdate(e);
            on("touchmove", doc, dragMove);
            on("mousemove", doc, dragMove);
            if (isFunc(event.start)) event.start(cacheValue, target, e);
            //return e.preventDefault(), false;
        }

        function dragMove(e) {
            dragUpdate(e);
            //return e.preventDefault(), false;
        }

        function dragStop(e) {
            drag = false;
            off("touchmove", doc, dragMove);
            off("mousemove", doc, dragMove);
            if (isFunc(event.stop)) event.stop(cacheValue, target, e);
            //return e.preventDefault(), false;
        }

        function dragUpdate(e) {
            e = e || win.event;
            var pos = e.touches ? e.touches[0][page] : e[page],
                move = edge(pos - rangerDistance, 0, rangerSize),
                value = edge(((pos - rangerDistance) / rangerSize) * 100, 0, 100);
            if (!pos) pos = e[client] + doc.body[scroll] + doc.documentElement[scroll];
            if (drag) {
                dragger.style[css] = (move - (draggerSize / 2)) + 'px';
                cacheValue = Math.round(value);
                if (isFunc(event.drag)) event.drag(cacheValue, target, e);
            }
        }

        on("touchstart", ranger, dragStart);
        on("mousedown", ranger, dragStart);

        on("touchend", doc, dragStop);
        on("mouseup", doc, dragStop);

        on("resize", win, function(e) {
            setSize(), drag = false;
            dragger.style[css] = (((cacheValue / 100) * rangerSize) - (draggerSize / 2)) + 'px';
        });

        return setSize(), dragInit(), {
            setPosition: setPosition,
            on: function (ev, func) {
                event[ev] = func;
            }
        };
    }

    function Q() {
        var self = this;
        this.options        = options || {};
        this.el             = el;
        this.$el            = $(el);
        this.src            = this.$el.data('src');
        this.playInterval   = null;
        this.hintTimeout    = null;

        this.player = new Howl({
            src: options && options.src ? options.src : this.src,
            autoplay: false,
            preload: this.options.force || false,
            loop: false,
            volume: 0.5
        });

        self._buildContainer();
        self._buildEvents();

        if (this.player.state() == 'loaded')
            this.onLoad();
    }

    Q.prototype._setTrackPosition = function ($container, pos)  {
        $container.find('.audio__line-current').css('width', pos + '%');
    }

    Q.prototype._buildContainer = function () {
        var self = this;
        this.$el.addClass('audio');
        this.$el.html('<div class="audio__progress">' +
            '<div class="audio__line"></div>' +
            '</div>' +
            '<div class="audio__volume">' +
            '<div class="audio__line"></div>' +
            '</div>');
        this.$loader = $('<div class="audio__loader">').html('<div class="loader"><span></span><span></span><span></span><span></span></div>').appendTo(this.$el);
        this.$button = $('<button type="button" class="audio__btn">').prependTo(this.$el);
        this.$progress = this.$el.find('.audio__progress');
        this.$progressLine = $('<div class="audio__line-current">').appendTo(this.$progress.find('.audio__line'));
        this.$progressDragger = $('<div class="audio__slider">').appendTo(this.$progress.find('.audio__line')).hide();
        this.$volume = this.$el.find('.audio__volume');
        this.$volumeLine = $('<div class="audio__line-current">').appendTo(this.$volume.find('.audio__line'));
        this.$volumeDragger = $('<div class="audio__slider">').appendTo(this.$volume.find('.audio__line'));
        this.$duration = $('<div class="audio__duration">').appendTo(this.$progress);
        this.$hint = $('<div class="audio__hint">').appendTo(this.$el);
        this.progressSlider = RS(this.$progress.find('.audio__line'));
        this.volumeSlider = RS(this.$volume.find('.audio__line'));

        this.volumeSlider.setPosition(this.player.volume() * 100);
        this._setTrackPosition(this.$volume, this.player.volume() * 100);
    };

    Q.prototype._buildEvents = function () {
        var self = this;

        this.player.on('play', function () { self.onPlay(); });
        this.player.on('pause', function () { self.onPause(); });
        this.player.on('end', function () { self.onEnd(); });
        this.player.on('load', function () { self.onLoad(); })
        this.$button.click(function () {
            if (self.player.playing())
                self.player.pause();
            else
                self.player.play();
        });

        this.volumeSlider.on('drag', function (v) {
            self.onVolumeChange(v);
        });

        this.progressSlider.on('drag', function (v) {
            self.onProgressChange(v);
        });

        var checkVisibleState = function () {
            var visibleArea = $(document).scrollTop() + window.innerHeight
            if (self.$el.offset().top < visibleArea && self.$el.offset().top + self.$el.height() > $(document).scrollTop() && self.player.state() == 'unloaded')
                self.player.load();
        }

        if (!this.options.force) {
            checkVisibleState();
            $(window).scroll(checkVisibleState);
        }
    }

    Q.prototype.showTooltip = function (content, $attach) {
        var self = this;
        clearTimeout(this.hintTimeout);
        this.$hint.html(content).show().insertBefore($attach);
        this.$hint.css({
            top: $attach.position().top - this.$hint.outerHeight() - ($attach.height() / 2),
            left: $attach.position().left - (this.$hint.width() / 2)
        })
        this.hintTimeout = setTimeout(function () {
            self.$hint.hide();
        }, 300);
    }

    Q.prototype.onLoad = function () {
        this.$duration.html(fancyTimeFormat(this.player.duration()));
        this.$loader.hide();
    }

    Q.prototype.onProgressChange = function (value) {
        var timeFormatted = fancyTimeFormat(this.player.seek());
        this._setTrackPosition(this.$progress, value);
        this.player.seek(this.player.duration() * (value / 100));
        this.$duration.html(timeFormatted);
        this.$progressDragger.show();
        this.showTooltip(timeFormatted, this.$progressDragger);
    }

    Q.prototype.onVolumeChange = function (value) {
        this._setTrackPosition(this.$volume, value);
        this.player.volume(value / 100);
        this.showTooltip(value + '%', this.$volumeDragger);
    }

    Q.prototype.onPlay = function () {
        var self = this;
        this.$el.addClass('is-playing');
        this.$progressDragger.show();
        this.playInterval = setInterval(function () {
            var progress = (self.player.seek() / self.player.duration()) * 100;
            self.$duration.html(fancyTimeFormat(self.player.seek()));
            self._setTrackPosition(self.$progress, progress);
            self.progressSlider.setPosition(progress);
        }, 100);
    }

    Q.prototype.onPause = function () {
        this.$el.removeClass('is-playing');
        clearInterval(this.playInterval);
    }

    Q.prototype.onEnd = function () {
        this.$el.removeClass('is-playing');
        clearInterval(this.playInterval);
        this.$duration.html(fancyTimeFormat(this.player.duration()));
        this._setTrackPosition(this.$progress, 0);
        this.$progressDragger.hide();
    }

    return new Q();
}